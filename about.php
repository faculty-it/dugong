<?php include 'includes/header.html'; ?>

<div class="container">

  <div class="jumbotron">
    <div class="row">
      <div class="col">
        <h1>About Us</h1>
        <p><a href="./contact">Contact us</a></p>


        <p>The Dugong fans team:</p>
      </div>
      <div class="col">
        <img src="/images/two_dugongs.jpg" style="width: 20em">
      </div>
    </div>
  </div>

    <div class="card-group">
      <!-- Main component for a primary marketing message or call to action -->
      <div class="card text-white bg-info mb-3">
        <dl class="card-body">
          <dt class="card-title">Richard Fuller</dt>
          <dd class="card-text">Product Owner</dd>
          <dd class="card-text"><i class="far fa-envelope-open"></i> richard.fuller@york.ac.uk</dd>
          <dd class="card-text"><i class="fab fa-twitter-square"></i> -</dd>
          <dd class="card-text"><i class="far fa-file-alt"></i> -</dd>
        </dl>
      </div>

      <div class="card text-white bg-info mb-3">
        <dl class="card-body">
          <dt class="card-title">Rick Taylor</dt>
          <dd class="card-text">Scrum Master</dd>
          <dd class="card-text"><i class="far fa-envelope-open"></i> rick.taylor@york.ac.uk</dd>
          <dd class="card-text"><i class="fab fa-twitter-square"></i> -</dd>
          <dd class="card-text"><i class="far fa-file-alt"></i> -</dd>
        </dl>
      </div>

      <div class="card text-white bg-info mb-3">
        <dl class="card-body">
          <dt class="card-title">Dan Bishop</dt>
          <dd class="card-text">Development Team</dd>
          <dd class="card-text"><i class="far fa-envelope-open"></i> dan.bishop@york.ac.uk</dd>
          <dd class="card-text"><i class="fab fa-twitter-square"></i> -</dd>
          <dd class="card-text"><i class="far fa-file-alt"></i> -</dd>
        </dl>
      </div>

      <div class="card text-white bg-info mb-3">
        <dl class="card-body">
          <dt class="card-title">Dan Miller</dt>
          <dd class="card-text">Development Team</dd>
          <dd class="card-text"><i class="far fa-envelope-open"></i> dan.miller@york.ac.uk</dd>
          <dd class="card-text"><i class="fab fa-twitter-square"></i> -</dd>
          <dd class="card-text"><i class="far fa-file-alt"></i> <a href="http://www.miller.cm/" style="color:white">http://www.miller.cm/</a></dd>
        </dl>
      </div>

      <div class="card text-white bg-info mb-3">
        <dl class="card-body">
          <dt class="card-title">Ben Jones</dt>
          <dd class="card-text">Development Team</dd>
          <dd class="card-text"><i class="far fa-envelope-open"></i> b.jones@york.ac.uk</dd>
          <dd class="card-text"><i class="fab fa-twitter-square"></i> -</dd>
          <dd class="card-text"><i class="far fa-file-alt"></i> -</dd>
        </dl>
      </div>

    </div>


      <div class="card">
        <h5 class="card-header"><i class="fas fa-football-ball"></i> Scrum</h5>
        <div class="card-body">
          <p class="card-text">
            Scrum is a framework within which people can address complex adaptive
            problems, while productively and creatively delivering products of the
            highest possible value. You can find out more at <a href="https://www.scrum.org">
            scrum.org</a>
          </p>

          <p class="card-text">
            We used the Scrum framework to aid in the development of this website.
            You can see the roles that each member of the team took above.
          </p>

          <p class="card-text">
            Our Scrum Master already had experience of working using Scrum and
            kept us all on track.
          </p>

          <p class="card-text">
            Our Product Owner liaised with other teams when the project expanded
            into a "Scrum Zoo". They also had overall control of which features we
            would include on any given sprint.
          </p>

          <p class="card-text">
            Sprints generally lasted 30 minutes. Tasks were divided amongst the development
            team during sprint planning and we had a "daily Scrum" meeting during
            each sprint to see where we were and how we might need to adapt in order
            to achieve our sprint goal.
          </p>

          <a href="https://www.scrum.org" class="btn btn-primary">scrum.org</a>
        </div>
      </div>




</div>

<?php include 'includes/footer.html'; ?>
