<?php include 'includes/header.html'; ?>

<div class="container">
  <div class="jumbotron">
    <h1>Animal Phaktz</h1>
    
    <img src="./images/dugong_1.jpg" class="img-fluid" alt="Responsive image">

    <p>
        They are sometimes called "sea cows" as they eat large amounts of sea grass. They live in warm, shallow areas where the sea grass grows. This area includes the north coast of Australia, and in other countries in the Indian Ocean and the Pacific Ocean.
    </p>
    <p>
        Dugongs are more closely related to elephants than to other sea creatures. Their closest aquatic relative is the manatee, a fresh water species found in America and West Africa.
    </p>
    <p>
    The dugong can grow to about 3 m (10 ft) long and weigh as much as 400 kg (882 lb). They only come to the surface to breathe, and unlike seals, they never come up on the land. A baby dugong is called a calf. It drinks milk from its mother until about two years old. A dugong reaches its adult size between the ages of 9 and 17 years. The dugong can live for up to 70 years of age. They are grey to brown in color. They have a tail with flukes, like a whale, and flippers. They do not have a dorsal fin like a shark. They have a wide flat nose, small eyes, and small ears.
    </p>

  </div>

</div>

<?php include 'includes/footer.html'; ?>
