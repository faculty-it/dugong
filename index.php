<?php include 'includes/header.html'; ?>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" id="home-jumbotron">
      <div class="container">
	<img src="images/dugong-logo.png" style="float:right; width: 35%; opacity: 0.8" />
        <h1 style="font-weight: bold !important">Scrum Zoo: dugongs!</h1>
        <h2><i>Dugong dugon</i></h2>
        <p>The dugong is a fascinating animal, you can learn more about it on this site.</p>
	      <p>
          <a class="btn btn-secondary btn-lg" role="button" href="/diet">
            <i class="fas fa-hamburger"></i>
            Learn more about its diet! &raquo;
          </a>
	</p>
	<p class="advert">This site is part of the <a href="/zoo.php">Scrum Zoo</a> network</p>
	      </div>
    </div>

    <div class="container mt-3">
      <!-- Example row of columns -->
        <div class="card-deck">
          <div class="card">
            <img class="card-img-top" src="./images/dugong_2.jpg" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">The habitat</h5>
              <p class="card-text">Dugongs live in the sea!</p>
            </div>
            <div class="card-footer">
              <p>
                <a class="btn btn-primary" role="button" href="habitat.php">
                  <i class="fas fa-home"></i> See the habitat &raquo;
                </a>
              </p>
            </div>
          </div>
          <div class="card">
            <img class="card-img-top" src="./images/dugong_3.jpg" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">See some pictures!</h5>
              <p class="card-text">Here are some beautiful pictures.</p>
            </div>
            <div class="card-footer">
              <p>
                <a class="btn btn-primary" role="button" href="gallery_1.php">
                  <i class="fas fa-camera-retro"></i> See the gallery &raquo;
                </a>
              </p>
            </div>
          </div>
          <div class="card">
            <img class="card-img-top" src="./images/dugong_4.jpg" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">Sexy times</h5>
              <p class="card-text">Some mating and reproduction facts about the animal.</p>
            </div>
            <div class="card-footer">
              <p>
                <a class="btn btn-primary" role="button" href="mating.php">
                  <i class="fas fa-bed"></i> View details &raquo;
                </a>
              </p>
            </div>
          </div>
        </div>

    </div>

<?php include 'includes/footer.html'; ?>
