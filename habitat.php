<?php include 'includes/header.html'; ?>

<div class="container">
  <div class="jumbotron">
    <h1>Animal Habitat</h1>

<p>
Dugongs are found in warm coastal waters from the western Pacific Ocean to the eastern coast of Africa, along an estimated 140,000 kilometres (86,992 mi) of coastline between 26° and 27° degrees to the north and south of the equator
</p>
<p>
Their historic range is believed to correspond to that of seagrasses from the Potamogetonaceae and Hydrocharitaceae families
</p>
<p>
The full size of the former range is unknown, although it is believed that the current populations represent the historical limits of the range, which is highly fractured. Today populations of dugongs are found in the waters of 37 countries and territories.
</p>

<h3>Where do Dugongs make their home?</h3>
<p>
Dugongs are generally found in warm waters around the coast with large numbers concentrated in wide and shallow protected bays. The dugong is the only strictly-marine herbivorous mammal, as all species of manatee utilise fresh water to some degree. Nonetheless, they can tolerate the brackish waters found in coastal wetlands, and large numbers are also found in wide and shallow mangrove channels and around leeward sides of large inshore islands, where seagrass beds are common. They are usually located at a depth of around 10 m (33 ft), although in areas where the continental shelf remains shallow dugongs have been known to travel more than 10 kilometres (6 mi) from the shore, descending to as far as 37 metres (121 ft), where deepwater seagrasses such as Halophila spinulosa are found. Special habitats are used for different activities. It has been observed that shallow waters are used as sites for calving, minimising the risk of predation. Deep waters may provide a thermal refuge from cooler waters closer to the shore during winter.

</p>

      </div>

    </div>

<?php include 'includes/footer.html'; ?>
