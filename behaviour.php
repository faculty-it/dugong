<?php include 'includes/header.html'; ?>
<div class="container">
<div class="jumbotron">
    <h1>Animal behaviour</h1>
<h2>The semi-nomadic dugong</h2>
<p>
Dugongs are almost like nomads.  Like a drunk student at 2am, they travel many miles to find food.  They will move in large groups, probably caused by changes in seagrass availability</p>
<h2>Protect the dugong family</h2>
<p>Dugongs have few natural predators, who would want to kill this magnificent beast? Crocodiles, killer whales, sharks and us, that's who.  They calve in shallow waters to minimise the risk from these nasty characters</p>
<h2>Goodnight Mr Dugong</h2>
<p>One of the mysteries of the dugong is how they sleep.  Literally nobody knows.</p> 
</div>
</div>
<?php include 'includes/footer.html'; ?>
