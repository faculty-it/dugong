<?php include 'includes/header.html'; ?>

<?php include 'includes/header.html'; ?>

<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // the message
  $msg = $_POST['enquiry'];

  // use wordwrap() if lines are longer than 70 characters
  $msg = wordwrap($msg,70);

  // set from address
  $headers = "From: $_POST[email]\r\n";

  // send email
  mail("dugong-group@york.ac.uk",$_POST['nature'],$msg,$headers);

?>

<div class="container">
  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-12">
      <h2>Thanks!</h2>
      <p>Thank you, we'll be in touch soon.</p>
    </div>
  </div>
</div>

<?php } else { ?>

  <div class="container">
    <!-- Example row of columns -->
    <div class="row">
            <div class="col-md-8">
              <h2>Contact Us</h2>
              <form method="post">
                <div class="form-group">
                  <label for="email">Your Email address</label>
                  <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else. Promise.</small>
                </div>
                <div class="form-group">
                  <label for="nature">Nature of Enquiry</label>
                  <select class="form-control" name="nature" id="nature">
                    <option>I want more info...</option>
                    <option>I'd like to buy a Dugong...</option>
                    <option>I've rescued a Dugong, what now?</option>
                    <option>Other...</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="enquiry">Enquiry</label>
                  <textarea class="form-control" id="enquiry" name="enquiry" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="card bg-light mt-5 mb-3">
                    <div class="card-header bg-success text-white text-uppercase"><i class="fa fa-home"></i> Address</div>
                    <div class="card-body">
                        <p>Room 211, Spring Lane Building</p>
                        <p>University of York</p>
                        <p>YO10 5DD</p>
                        <p>Email : richard.fuller@york.ac.uk</p>
                        <p>Tel.  : 01904 32 5678</p>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php } ?>

<?php include 'includes/footer.html'; ?>
