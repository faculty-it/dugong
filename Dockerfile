FROM php:7-apache

RUN apt-get update && apt-get upgrade

COPY .docker/vhost.conf /etc/apache2/sites-available/000-default.conf

EXPOSE 80

RUN a2enmod rewrite

ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_RUN_DIR /var/run
ENV APACHE_PID_FILE /var/run/apache.pid
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data

#COPY . /srv/app
#RUN chown -R www-data:www-data /srv/app

CMD ["/usr/sbin/apache2", "-D",  "FOREGROUND"]

