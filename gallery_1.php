<?php include 'includes/header.html'; ?>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12">
          <h2>Dugong Drawing by Rick - Aged 5 1/2 from York</h2>
          <img src="./images/Rick_Aged_5.jpg" width="900" class="img-fluid" alt="Responsive image">
          <p>Dugongs are pretty.</p>
	  <p>Do you have pretty dugong pictures? Submit yours to pics@dugong.york.ac.uk</p>
          <?php include 'includes/gallery_nav.html'; ?>
        </div>
      </div>
    </div>

<?php include 'includes/footer.html'; ?>
