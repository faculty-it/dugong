<?php include 'includes/header.html'; ?>
  <div class="container">
    <div class="jumbotron">
      <h1>Diet</h1>
      <li>Dugongs eat seagrasses.</li>
      <li>Dugongs eat a lot of seagrass, <a href="https://www.iucn.org/sites/dev/files/book_dugong_and_their_seagrass_habitat.pdf">up to 40kg every day!</a></li>
      <li>Dugongs <a href="https://www.nationalgeographic.com/animals/mammals/d/dugong/">graze, day and night.</a> They never stop eating!</li>
    </div>
  </div>
<?php include 'includes/footer.html'; ?>

