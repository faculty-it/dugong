<?php include 'includes/header.html'; ?>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12">
	  <h2>3-Dugong</h2>
<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/17bdf341fcc047888935f67a05dd2fea/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/3d-models/dugong-sea-cow-17bdf341fcc047888935f67a05dd2fea?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dugong (Sea Cow)</a>
    by <a href="https://sketchfab.com/ianchrisjohn?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Ian Christopher</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
          <p>Dugongs exist in at least 3 dimensions! (Some believe they can travel through time and exist in parallel dimensions too.)</p>
	  <p>Do you have pretty dugong pictures? Submit yours to pics@dugong.york.ac.uk</p>
<?php include 'includes/gallery_nav.html'; ?>

        </div>
      </div>
    </div>

<?php include 'includes/footer.html'; ?>
