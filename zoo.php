
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="google-site-verification" content="OZG44JMwv0VkH2qd76rxYCCFNYtHlW2Po8p6p30pMws" />
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>The Scrum Zoo</title>

    <style>
    BODY {background-color: white !important;}
      .card {width: 30em !important;
             height: 30em !important;}
             IMG {width: 25em !important;
              height: 20em !important;}
              .jumbotron {color: black !important; background-image: none !important;}
           </style>


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="assets/css/jumbotron.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="container">

     <div class="jumbotron" id="zoo-jumbotron">
       <div class="container">
          <h1 style="font-weight: bold !important">The Scrum Zoo</h1>
          <p>Welcome to the world's weirdest zoo! Check out our animals below...</p>
       </div>
     </div>

     <div class="row">
        <div class="col">
          <div class="card bg-info" style="">
            <div class="text-center"><a href="/"><img class="card-img-top" src="img/dugong.jpg" alt="Dugong"></a></div>
            <div class="card-body">
              <p class="card-text">Did you know? The <strong>Dugong</strong> is a <strong>Herbivore Mammal</strong>. They are collectively known as a <strong>herd</strong>, has a lifespan of <strong>70 years</strong> and weighs between <strong>510 to 1,100lbs</strong>. </p>
            </div>
          </div>
        </div>

         <div class="col">
           <div class="card bg-info" style="">
             <div class="text-center"><a href="https://sites.google.com/a/york.ac.uk/ottersaregreat/"><img class="card-img-top" src="img/otter.jpg" alt="Otter"></a></div>
             <div class="card-body">
               <p class="card-text">Retractable claws on a sea otter’s front paws allow the sea otter to grab food!</p>
             </div>
           </div>
         </div>
         </div>
         <div class="row">
         <div class="col">
           <div class="card bg-info" style="opacity: 0.5">
             <div class="text-center"><a href="#"><img class="card-img-top" src="img/guineapig.jpg" alt="Guinea pig"></a></div>
             <div class="card-body">
               <p class="card-text">Coming soon: Exciting information about guinea pigs...</p>
             </div>
           </div>
         </div>

         <div class="col">
           <div class="card bg-info" style="opacity: 0.5">
             <div class="text-center"><a href="#"><img class="card-img-top" src="img/armadillo.jpg" alt="Armadillo"></a></div>
             <div class="card-body">
               <p class="card-text">Soft on the inside, crunchy on the outside. Armadillos!</p>
             </div>
           </div>
         </div>
       </div>


      <footer>
       <p>&copy; 2019 The Scrum Zoo - <a href="https://www.york.ac.uk/about/legal-statements/">Legal disclaimer</a></p>
     </footer>
   </div> <!-- /container -->


   <!-- Bootstrap core JavaScrip

   ================================================== -->
   <!-- Placed at the end of the document so the pages load faster -->
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
 </body>
</html>
