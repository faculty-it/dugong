<?php include 'includes/header.html'; ?>

<div class="container">

  <div class="jumbotron">
    <div class="row">
      <div class="col">
        <h1>Mating facts!</h1>
        <p>Here are some fascinating facts about the bedroom habits of the Dugong.</p>
      </div>
      <div class="col">
        <img src="/images/two_dugongs.jpg" style="width: 20em">
      </div>
    </div>
  </div>
      <!-- Main component for a primary marketing message or call to action -->
      <div class="card text-white bg-info mb-3">
        <dl class="card-body">
          <dt class="card-title">What is the gestational period of the animal?</dt>
          <dd class="card-text">12 months.</dd>
        </dl>
      </div>

      <div class="card text-white bg-info mb-3">
        <dl class="card-body">
          <dt class="card-title">How many offspring does it typically have at a time?</dt>
          <dd class="card-text">One. That's enough.</dd>
        </dl>
      </div>

      <div class="card text-white bg-info mb-3">
        <dl class="card-body">
          <dt class="card-title">How long do the offspring live with the parent?</dt>
          <dd class="card-text">About a year and a half.</dd>
        </dl>
      </div>

</div>

<?php include 'includes/footer.html'; ?>
