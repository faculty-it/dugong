<?php include 'includes/header.html'; ?>

  <div class="container">

    <div class="jumbotron">
      <div class="row">
        <div class="col">
          <h1>It's all about the anatomy...</h1>
          <p>Size, shape, breathing and living... it's all here...</p>
        </div>
        <div class="col">
          <img src="/images/dugong_2.jpg" style="width: 20em">
        </div>
      </div>
    </div>

    <div class="card text-white bg-info" >
      <div class="card-body">
        <h5 class="card-title">Are they big?</h5>
        <p class="card-text">Dugongs are large marine mammals. They can grow up to 3 m (9.8 ft) in length and they weigh between 250 and 300 Kgs (551 - 661 lbs). They have a large body with thick, smooth skin.</p>
        <a href="http://www.theanimalfiles.com/mammals/dugong_manatees/dugong.html" class="card-link">Source</a>
      </div>
    </div>

    <div class="card text-white bg-info" >
      <div class="card-body">
        <h5 class="card-title">But how do they breathe?</h5>
        <p class="card-text">Dugongs, like other mammals, must surface to breath, but they cannot hold their breath for very long. Their nostrils are situated on the top of their snout and they are able to close them when they go underwater.</p>
        <a href="http://www.theanimalfiles.com/mammals/dugong_manatees/dugong.html" class="card-link">Source</a>
      </div>
    </div>
    <div class="card text-white bg-info" >
      <div class="card-body">
        <h5 class="card-title">And how long do they live?</h5>
        <p class="card-text">Dugongs live to 70, unless someone kills them, which happens far too often. :(</p>
        <a href="https://en.wikipedia.org/wiki/Dugong" class="card-link">Source</a>
      </div>
    </div>
  </div>

<?php include 'includes/footer.html'; ?>
